var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

var app = express();
app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

var dbUrl = 'mongodb://dbuser:dbpassword1@ds215370.mlab.com:15370/chat-database';
var Message = mongoose.model('Message', {
  name: String,
  message: String
});

mongoose.connect(dbUrl , (err) => { 
  console.log('mongodb connected',err);
});

app.get('/messages', (req, res) => {
  Message.find({}, (err, messages) => {
    res.send(messages);
  });
});

app.post('/messages', (req, res) => {
  var message = new Message(req.body);
  message.save((err) => {
    if(err) {
      sendStatus(500);
    }
    res.sendStatus(200);
  });
});

var server = app.listen(3000, () => {
  console.log(`Server is running on port ${server.address().port}`);
});
